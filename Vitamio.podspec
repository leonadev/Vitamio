Pod::Spec.new do |s|
  s.name         = "Vitamio"
  s.version      = "4.2.0"
  s.summary      = "Vitamio iOS SDK"
  s.description  = <<-DESC
                   Vitamio iOS SDK
                   DESC
  s.homepage     = "https://gitlab.com/leonadev/Vitamio"
  s.license      = 'MIT'
  s.author       = "leonadev"
  s.platform     = :ios, "8.0"
  s.source       = { :git => "https://gitlab.com/leonadev/Vitamio.git", :tag => s.version.to_s }
  s.source_files  = "Vitamio", "Vitamio/include/Vitamio/*.h"
  s.public_header_files = "Vitamio/include/Vitamio/*.h"
  s.vendored_libraries = "Vitamio/*.a"
  s.frameworks = "Foundation","UIKit","AVFoundation","AudioToolbox","CoreGraphics","CoreMedia","CoreVideo","MediaPlayer","OpenGLES","QuartzCore"
  s.libraries = "bz2", "z","stdc++","iconv"
  s.pod_target_xcconfig = { 'OTHER_LDFLAGS' => '-lObjC' }
  s.requires_arc = true
end
